<?php
/**
 * @file
 * Page callbacks for the content taxonomy jsTree widget.
 */

/**
 * JSON data callback.
 */
function content_taxonomy_jstree_page_data($vid, $parent = 0) {
  // Allow the AJAX call to provide a different parent via get parameters.
  if ($_REQUEST['parent'] && is_numeric($_REQUEST['parent'])) {
    $parent = $_REQUEST['parent'];
  }

  $data = _content_taxonomy_jstree_tree_data($vid, $parent);

  // We are returning JSON, so tell the browser.
  drupal_set_header('Content-Type', 'application/json');
  echo json_encode($data);
}

/**
 * Helper function to load the tree data for a vocabulary.
 *
 * @param $vid
 *   The vocabulary we're getting tree data for.
 * @param $parent
 *   Parent term to get children for. 0 for the root.
 */
function _content_taxonomy_jstree_tree_data($vid, $parent = 0) {
  $cache_key = 'tree_data:' . $vid . ':' . $parent;
  // Return data from cache if available.
  $cache = ($reset) ? FALSE : cache_get($cache_key, CONTENT_TAXONOMY_JSTREE_CACHE_BIN);
  if ($cache && !empty($cache->data)) {
    $data = $cache->data;
  }
  // Get the data from the database.
  else {
    $data = array();

    $tree = taxonomy_get_tree($vid, $parent, -1, 1);

    // Make a data object for each term.
    foreach ($tree as $term) {
      $data[] = array(
        'attr' => array('id' => 'term_' . $term->tid),
        'data' => array(
          'title' => $term->name,
        ),
        'metadata' => array('tid' => $term->tid),
        'state' => (_content_taxonomy_jstree_term_has_children($term->tid)) ? 'closed' : 'leaf',
      );
    }

    cache_set($cache_key, $data, CONTENT_TAXONOMY_JSTREE_CACHE_BIN);
  }

  return $data;
}

/**
 * Quick callback to check if term has children.
 *
 * This is used to determine the node type selected in the tree (closed
 * or leaf node).
 */
function _content_taxonomy_jstree_term_has_children($tid) {
  $query = db_query("
    SELECT tid FROM {term_data} AS td
    INNER JOIN {term_hierarchy} AS th USING (tid)
    WHERE parent = %d
    LIMIT 1
  ", array(':tid' => $tid));

  return db_result($query);
}

