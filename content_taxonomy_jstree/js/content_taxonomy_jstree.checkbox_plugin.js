/*
 * Modified version of 
 * jsTree checkbox plugin 1.0
 * Inserts checkboxes in front of every node
 * Depends on the ui plugin
 * DOES NOT WORK NICELY WITH MULTITREE DRAG'N'DROP
 *
 * Alterations
 * Add support for marking a checkbox as implicitly selected upon created
 * Add support for marking a checkbox as selected upon creation
 * Adds support for disabling a checkbox upon creation.
 */
(function ($) {
  $.jstree.plugin("content_taxonomy_checkbox", {
    __init : function () {
      this.select_node = this.deselect_node = this.deselect_all = $.noop;
      this.get_selected = this.get_checked;

      this.get_container()
        .bind("open_node.jstree create_node.jstree clean_node.jstree", $.proxy(function (e, data) { 
          this._prepare_checkboxes(data.rslt.obj);
        }, this))
        .bind("loaded.jstree", $.proxy(function (e) {
          this._prepare_checkboxes();
        }, this))
        .delegate("a", "click.jstree", $.proxy(function (e) {
          var a = $(e.target);
          var clickEvent
          if (a.is('a') && a.find('> .jstree-checkbox').length == 0) {
            e.preventDefault();
            return;
          }
          if(this._get_node(e.target).hasClass("jstree-checked")) { this.uncheck_node(e.target); }
          else { this.check_node(e.target); }
          if(this.data.ui) { this.save_selected(); }
          if(this.data.cookies) { this.save_cookie("select_node"); }
          e.preventDefault();
        }, this));
    },
    __destroy : function () {
      this.get_container().find(".jstree-checkbox").remove();
    },
    _fn : {
      _prepare_checkboxes : function (obj) {
        obj = !obj || obj == -1 ? this.get_container() : this._get_node(obj);
        var c, _this = this, t, settings = this.get_settings(), hideCheckboxes;
        hideCheckboxes = Number(settings.hideCheckboxes);
        obj.each(function () {
          t = $(this);
          if (t.is("li")) {
            t.addClass('content-taxonomy-checkbox');
          }
          c = t.is("li") && t.hasClass("jstree-checked") ? "jstree-checked" : "jstree-unchecked";
          var jstree_data = t.data('jstree');
          if (hideCheckboxes > 0 && hideCheckboxes > t.parents('.content-taxonomy-checkbox').length && !t.hasClass('jstree-leaf')) {
            t.removeClass("jstree-checked jstree-unchecked").addClass('jstree-undetermined');
          }
          else {
            t.find("a").not(":has(.jstree-checkbox)").prepend("<ins class='jstree-checkbox'>&#160;</ins>").parent().not(".jstree-checked, .jstree-unchecked").addClass(c);
          }
          if (
            typeof jstree_data === 'object' &&
            typeof settings.initially_open[jstree_data.tid] !== 'undefined' &&
            settings.initially_open[jstree_data.tid] === true &&
            !t.parent('ul').parent('li').hasClass("jstree-unchecked")
          ) {
            t.removeClass("jstree-unchecked jstree-undetermined").addClass('jstree-checked');
            settings.initially_open[jstree_data.tid] = false;
          }
          if (typeof jstree_data === 'object' && settings.implicitSelected[jstree_data.tid] === true) {
            settings.implicitSelected[jstree_data.tid] = false;
            if (!t.hasClass("jstree-checked") && !$(this).parent("ul").parent("li").hasClass("jstree-unchecked")) {
              t.removeClass("jstree-unchecked").addClass("jstree-undetermined");
            }
          }
           // Trigger the creation of the checkbox, Drupal hook style.
          t.trigger('contentTaxonomyCheckboxCreation');
        });
        // Update setings to mark which checkboxes has been marked.
        this._set_settings(settings);
      },
      change_state : function (obj, state) {
        obj = this._get_node(obj);
        if (obj.is("a") && obj.find(" > .jstree-checkbox").length === 0) {
          this.__callback(obj);
          return;
        }
        state = (state === false || state === true) ? state : obj.hasClass("jstree-checked");
        if(state) { obj.find("li").andSelf().removeClass("jstree-checked jstree-undetermined").addClass("jstree-unchecked"); }
        else {
          obj.find("li").andSelf().removeClass("jstree-unchecked jstree-undetermined").addClass("jstree-checked"); 
          if(this.data.ui) { this.data.ui.last_selected = obj; }
          this.data.content_taxonomy_checkbox.last_selected = obj;
        }
        obj.parentsUntil(".jstree", "li").each(function () {
          var $this = $(this);
          if(state) {
            if($this.children("ul").children(".jstree-checked, .jstree-undetermined").length) {
              $this.parentsUntil(".jstree", "li").andSelf().removeClass("jstree-checked jstree-unchecked").addClass("jstree-undetermined");
              return false;
            }
            else {
              $this.removeClass("jstree-checked jstree-undetermined").addClass("jstree-unchecked");
            }
          }
          else {
            if($this.children("ul").children(".jstree-unchecked, .jstree-undetermined").length) {
              $this.parentsUntil(".jstree", "li").andSelf().removeClass("jstree-checked jstree-unchecked").addClass("jstree-undetermined");
              return false;
            }
            else {
              $this.removeClass("jstree-unchecked jstree-undetermined").addClass("jstree-checked");
            }
          }
        });
        if(this.data.ui) { this.data.ui.selected = this.get_checked(); }
        this.__callback(obj);
      },
       check_node : function (obj) {
         this.change_state(obj, false);
       },
       uncheck_node : function (obj) {
         this.change_state(obj, true);
       },
       check_all : function () {
         var _this = this;
         this.get_container().children("ul").children("li").each(function () {
           _this.check_node(this, false);
         });
       },
       uncheck_all : function () {
         var _this = this;
         this.get_container().children("ul").children("li").each(function () {
           _this.change_state(this, true);
         });
       },

       is_checked : function(obj) {
         obj = this._get_node(obj);
         return obj.length ? obj.is(".jstree-checked") : false;
       },
       get_checked : function (obj) {
         obj = !obj || obj === -1 ? this.get_container() : this._get_node(obj);
         return obj.find("> ul > .jstree-checked, .jstree-undetermined > ul > .jstree-checked");
       },
       get_unchecked : function (obj) {
         obj = !obj || obj === -1 ? this.get_container() : this._get_node(obj);
         return obj.find("> ul > .jstree-unchecked, .jstree-undetermined > ul > .jstree-unchecked");
       },

       show_checkboxes : function () { this.get_container().children("ul").removeClass("jstree-no-checkboxes"); },
       hide_checkboxes : function () { this.get_container().children("ul").addClass("jstree-no-checkboxes"); },

       _repair_state : function (obj) {
         obj = this._get_node(obj);
         if(!obj.length) { return; }
         var a = obj.find("> ul > .jstree-checked").length,
           b = obj.find("> ul > .jstree-undetermined").length,
           c = obj.find("> ul > li").length;

         if(c === 0) { if(obj.hasClass("jstree-undetermined")) { this.check_node(obj);  } }
         else if(a === 0 && b === 0) { this.uncheck_node(obj); }
         else if(a === c) { this.check_node(obj); }
         else {
           obj.parentsUntil(".jstree","li").removeClass("jstree-checked jstree-unchecked").addClass("jstree-undetermined");
         }
       },
       reselect : function () {
         if(this.data.ui) {
           var _this = this,
             s = this.data.ui.to_select;
           s = $.map($.makeArray(s), function (n) { return "#" + n.toString().replace(/^#/,"").replace('\\/','/').replace('/','\\/'); });
           this.deselect_all();
           $.each(s, function (i, val) { _this.check_node(val); });
           this.__callback();
         }
       }
     }
   });
})(jQuery);
