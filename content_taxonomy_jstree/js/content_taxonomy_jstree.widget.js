/**
 * @file
 * JavaScript prototype and Drupal behaviors for managing tree browser.
 */

/**
 * Prototype for jsTree widget controller.
 */
Drupal.ContentTaxonomyJSTreeWidget = function (data) {
  var self = this;
  self.data = data;

  /**
   * Constructor for our widget controller.
   */
  self.init = function () {
    // Init should only be run once.
    if (self.initialised) { return; }
    self.initialised = true;

    // Add event to display checkbox children when selected
    if (self.data.expandSelectedCheckboxes) {
      // Event for clicking on the checkbox.
      $("ins.jstree-checkbox").live('click', function(e) {
        var clickEvent;
        // Trigger the click.jstree, but only when it's closed and when it's selected
        if ($(this).parent().parent().hasClass('jstree-closed') && $(this).parent().parent().hasClass('jstree-checked')) {
          clickEvent = jQuery.Event('click.jstree');
          // The event need the pageY value for it to function properly.
          clickEvent.pageY = 0;
          $(this).parent().prev().trigger(clickEvent);
        }
        $(this).parent("a").addClass('clicked');
      });
      // Event for clicking on the pseudo link.
      $(".content-taxonomy-checkbox a").live('click', function(evt) {
        if (!$(this).hasClass('clicked')) {
          var clickEvent;
          clickEvent = jQuery.Event('click.jstree');
          // The event need the pageY value for it to function properly.
          clickEvent.pageY = 0;
          $(this).prev().trigger(clickEvent);
        }
        else {
          $(this).removeClass('clicked');
        }
      });
    }

    // Hidden field to contain the choice data.
    self.dataField = $('#content-taxonomy-jstree-' + data.autoid);

    // Figure out previously selected terms.
    var selected = {};

    $.each(self.data.selected, function (i, value) {
      selected[value] = true;
    });

    // Set up the div wrapper for the jsTree.
    self.wrapper = $('<div></div>');
    self.wrapper.insertBefore(self.dataField);

    // Set up the jsTree instance.
    self.wrapper.jstree({
      'initially_open': selected,
      'implicitSelected': self.data.implicitSelected,
      'hideCheckboxes': self.data.hideCheckboxes,
      'json_data': {
        'ajax': {
          'url': data.dataURL,
          // this function is executed in the instance's scope (this refers to the tree instance)
          // the parameter is the node being loaded (may be -1, 0, or undefined when loading the root nodes)
          "data" : function (n) {
            // the result is fed to the AJAX request `data` option
            return {
              "parent": n.attr ? n.attr("id").replace("term_", "") : 0
            };
          }
        },
        'progressive_render': false
      },
      "plugins" : ["themes", "json_data", "content_taxonomy_checkbox"],
      "themes" : {"theme" : "default",
        "icons": false,
        "url" : Drupal.settings.basePath + self.data.themeUrl
      }
    })
    // Set up a handler for each time the tree is clicked. This updates
    // the stored state in the dataField.
    .delegate("a", "click.jstree", function (evt) {
      var tids = [];
      self.wrapper.jstree('get_checked').each(function () {
        var tid = Number(this.id.replace("term_", ""));
        if (tid && tid > 0) {
          tids.push(tid);
        }
      });

      // Add saved selections that aren't present in the tree.
      $.each(self.data.selected, function(i, tid) {
        if (tid && tid > 0 && $("#term_" + tid).length === 0) {
          tids.push(tid);
        }
      });

      // Store the selections comma-separated in the data field.
      self.dataField.val(tids.join(','));
    });
  };

  self.init();
  return self;
};

/**
 * Drupal JavaScript behavior to configure our jsTree widgets.
 */
Drupal.behaviors.contentTaxonomyJSTree = function () {
  $.each(Drupal.settings.content_taxonomy_jstree, function (autoid, data) {
    if (!data.processed) {
      var widget = new Drupal.ContentTaxonomyJSTreeWidget(data);
      data.processed = true;
    }
  });
};

